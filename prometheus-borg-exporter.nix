# SPDX-FileCopyrightText: 2023 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later
{
  config,
  pkgs,
  lib,
  flakeInputs,
  hostName,
  ...
}:
with lib; let
  cfg = config.services.prometheus-borg-exporter;

  toml = pkgs.formats.toml {};
in {
  options.services.prometheus-borg-exporter = {
    enable = mkEnableOption "Prometheus Push Exporter for BorgBackup";

    package = mkOption {
      type = types.package;
      default = pkgs.prometheus-borg-exporter;
      defaultText = "pkgs.prometheus-borg-exporter";
    };

    execInterval = mkOption {
      type = types.str;
      default = "*-*-* 00/4:00:00";
    };

    target = mkOption {
      type = types.str;
    };
    gateway = mkOption {
      type = types.str;
    };

    sshConfig = mkOption {
      type = with types; nullOr str;
      default = null;
    };

    keyfile = mkOption {
      type = with types; nullOr str;
      default = null;
    };

    unknownUnencryptedRepoAccessOk = mkOption {
      type = types.bool;
      default = false;
    };
    hostnameIsUnique = mkOption {
      type = types.bool;
      default = true;
    };
    borgBaseDir = mkOption {
      type = types.nullOr types.str;
      default = "/var/lib/borg";
    };

    config = mkOption {
      type = types.submodule {
        freeformType = toml.type;

        options = {
          groups = mkOption {
            type = types.listOf (types.submodule {
              freeformType = toml.type;

              options = {
                regex = mkOption {
                  type = types.str;
                };
              };
            });
          };
        };
      };

      default = {};
    };
  };

  config = mkIf cfg.enable {
    systemd.services."prometheus-borg-exporter" = {
      description = "Push information about BorgBackup state to prometheus";

      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${cfg.package}/bin/prometheus-borg-exporter ${escapeShellArgs [
          "--target=${cfg.target}"
          "--gateway=${cfg.gateway}"
          (toml.generate "prometheus-borg-exporter.toml" cfg.config)
        ]}";
        User = "borg";
        Group = "borg";

        Environment =
          optionals (!(isNull cfg.borgBaseDir)) [
            "BORG_BASE_DIR=${cfg.borgBaseDir}"
            "BORG_CONFIG_DIR=${cfg.borgBaseDir}/config"
            "BORG_CACHE_DIR=${cfg.borgBaseDir}/cache"
            "BORG_SECURITY_DIR=${cfg.borgBaseDir}/security"
            "BORG_KEYS_DIR=${cfg.borgBaseDir}/keys"
          ]
          ++ optional cfg.unknownUnencryptedRepoAccessOk "BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes"
          ++ optional cfg.hostnameIsUnique "BORG_HOSTNAME_IS_UNIQUE=yes"
          ++ optional (!(isNull cfg.sshConfig)) "BORG_RSH=\"${pkgs.openssh}/bin/ssh -F ${pkgs.writeText "config" cfg.sshConfig}\""
          ++ optional (!(isNull cfg.keyfile)) "BORG_KEY_FILE=${cfg.keyfile}";
      };
    };

    systemd.timers."prometheus-borg-exporter" = {
      wantedBy = ["timers.target"];
      timerConfig = {
        OnCalendar = cfg.execInterval;
        Persistent = true;
      };
    };
  };
}
