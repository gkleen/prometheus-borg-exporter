# SPDX-FileCopyrightText: 2023-2024 Gregor Kleen
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os, sys

from prometheus_client import CollectorRegistry, Gauge, push_to_gateway, generate_latest
import subprocess

import re

import argparse

from dataclasses import dataclass
from itertools import chain, repeat

from datetime import datetime

from pathlib import Path

import toml
import json

from frozendict import frozendict


@dataclass(eq=True, order=True, frozen=True)
class Group:
    regex: re.Pattern


@dataclass
class GroupState:
    count: int
    latest: datetime


def main():
    parser = argparse.ArgumentParser(
        prog="prometheus-borg-exporter",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--target", metavar="REPO", required=True, help="Borg repository"
    )
    parser.add_argument("--gateway", metavar="ADDR", help="Prometheus PushGateway")
    parser.add_argument("config", type=Path, metavar="FILE", help="Configuration file")
    args = parser.parse_args()

    config = toml.load(args.config)

    groups = list()
    for group in config.get("groups", []):
        regex = group.get("regex", None)
        if not regex:
            continue

        groups.append(Group(regex=re.compile(regex)))
    group_labels = list(
        set(chain.from_iterable([group.regex.groupindex.keys() for group in groups]))
    )

    registry = CollectorRegistry()
    last_run = Gauge(
        "job_last_success_unixtime",
        "Last time borg was successfully scraped",
        registry=registry,
    )
    ungrouped_count = Gauge(
        "archives_ungrouped_count",
        "Count of archives not matching any group",
        registry=registry,
    )
    backup_count = Gauge(
        "archive_group_count",
        "Count of archives matching group",
        registry=registry,
        labelnames=group_labels,
    )
    latest_backup = Gauge(
        "archive_group_latest_unixtime",
        "Timestamp of latest archive matching group",
        registry=registry,
        labelnames=group_labels,
    )

    states = dict()
    ungrouped = 0
    with subprocess.Popen(
        ["borg", "list", "--info", "--lock-wait=600", "--json", args.target],
        stdout=subprocess.PIPE,
    ) as proc:
        for archive in json.load(proc.stdout)["archives"]:
            for ix, group in enumerate(groups):
                if match := group.regex.fullmatch(archive["barchive"]):
                    k = (ix, frozendict(match.groupdict(default="")))
                    ts = datetime.fromisoformat(archive["time"])
                    if not k in states:
                        states[k] = GroupState(count=1, latest=ts)
                    else:
                        states[k].count += 1
                        if ts > states[k].latest:
                            states[k].latest = ts
                    break
            else:
                ungrouped += 1

    ungrouped_count.set(ungrouped)
    for (_, labels), state in states.items():
        backup_count.labels(**labels).set(state.count)
        latest_backup.labels(**labels).set(state.latest.timestamp())
    last_run.set_to_current_time()

    if args.gateway:
        push_to_gateway(
            args.gateway, job=config.get("job", args.target), registry=registry
        )
    else:
        sys.stdout.buffer.write(generate_latest(registry=registry))


if __name__ == "__main__":
    sys.exit(main())
